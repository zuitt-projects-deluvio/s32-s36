App: Booking System API

Descripton: 
Allows a user to enroll to a course.
Allows an admin to do CRUD operations on courses.
Allows us to register regular users.

User
FirstName - string
lastName - string
email - string
password - string
mobileNo - string
isAdmin - boolean,
		default: false

enrollments : [
	{
		courseID: string,
		status: string,
		dateEnrolled: date
	}
]


Associative Entity:

Enrollment-
Two-way Embedding when there is a many to many relationship. It means the associative entity created must be embedded on both documents.

Course:
name - string,
description - string,
price - number,
isActive - boolean
		default: true
createdOn - date,
enrollees: [
	{
		userID: string,
		status: string,
		dateEnrolled: date
	}
]