// import bcrypt module
const bcrypt = require('bcrypt')

// import User model
const Course = require("../models/Course");

// import auth module
const auth = require("../auth");


// Controllers


// Create new course
module.exports.addCourse = (req, res) => {

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	})

	newCourse.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
}

// Retrieve all courses
module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

// Retrieve a single course
module.exports.getSingleCourse = (req, res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

// Archive a course
module.exports.archiveCourse = (req, res) => {

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err))
};

// Activate Course
module.exports.activateCourse = (req, res) => {

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err))

};

// Retrieve Active Courses
module.exports.getActiveCourse = (req, res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

// Retrieve Active Courses
module.exports.getInactiveCourse = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};


// Update Course
module.exports.updateCourse = (req, res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	};

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


// find courses by name

module.exports.findCoursesByName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send("No courses found")
		} else {
			return res.send(result)
		}

	})
	.catch(err => res.send(err))
}


// find course by price

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({price: req.body.price})
	.then(result => {

		console.log(result);

		if(result.length === 0){
			return res.send("No courses found")
		} else {
			return res.send(result)
		}

	})
	.catch(err => res.send(err))

}

// get course enrollees

module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))
	.catch(err => res.send(err))

}