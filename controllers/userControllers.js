// import bcrypt module
const bcrypt = require('bcrypt')

// import User model
const User = require("../models/User");


// import auth module
const auth = require("../auth");


// import Course model
const Course = require("../models/Course");

// Controllers


// User registration
// console.log(module.exports)
module.exports.registerUser = (req, res) => {
	// console.log(req.body);

	/*
		bcrypt = will add a layer of security to your user's password

		this will hash out password into a randomized character version of the original string

		syntax: bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		Salt-rounds are the number of times the characters in the hash are randomized
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);


	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
}

//

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}


// Login

module.exports.loginUser = (req, res) => {

	// console.log(req.body);

	/*
		1. Find by user email
		2. if a user email is found, check the password
		3. if we dont find the user email, send a message
		4. if upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("User does not exist.")
		} else {
			// console.log(foundUser)
			const isPasswordCorrect = bcrypt.compareSync(
				req.body.password, foundUser.password)

			if(isPasswordCorrect){
				return res.send({accessToken: auth.
					createAccessToken(foundUser)})
			} else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));

};

module.exports.getUserDetails = (req, res) => {

	// console.log(req.user)

	// 1. Find a logged in user's document from our db and send it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

module.exports.checkEmailExists = (req, res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send("Email is available")
		} else {
			return res.send("Email is already registered!")
		}

	})
	.catch(err => res.send(err));

}

 module.exports.updateAdmin = (req, res) => {

 	console.log(req.params.id)
 	console.log(req.user.id)


 	let updates = {
 		isAdmin: true
 	}

 	User.findByIdAndUpdate(req.params.id, updates, {new: true})
 	.then(updatedUser => res.send(updatedUser))
 	.catch(err => res.send(err))

 }


 module.exports.updateUserDetails = (req, res) => {

 	console.log(req.body);
 	console.log(req.user);

 	let updates = {
 		firstName: req.body.firstName,
 		lastName: req.body.lastName,
 		mobileNo: req.body.mobileNo
 	}

 	User.findByIdAndUpdate(req.user.id, updates, {new: true})
 	.then(updatedUser => res.send(updatedUser))
 	.catch(err => res.send(err))

 }


 //enroll a user

 	/*
		Enroll Process:

		1. Look for the user by its ID
			> push the details of the course that we are trying to enroll in.
				> we'll push to a new enrollment subdocument in our user

		2. Look for the course by its id
			> push the details of the enrollee/user who's trying to enroll
				> we'll push to a new enrollees subdocument in our course

		3. When both saving documents are successful, we send a message to the client

 	*/

 module.exports.enroll = async (req, res) => {


 	console.log(req.user.id)
 	console.log(req.body.courseId)

 	// Checking if a user is an admin, if he is an admin he should not be able to enroll

 	if(req.user.isAdmin){
 		return res.send("Action Forbidden")
 	}

 	/*
		Find the user:

		async - a keyword that allows us to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line, it will allow us to wait for the result of the function

		await - a keyword that allows
 	*/


 	let isUserUpdated = await User.findById(req.user.id)
 	.then(user => {

 		console.log(user);

 		// add the courseId in an object and push that object into user's enrollment array
 		let newEnrollment = {
 			courseId: req.body.courseId
 		}

 		user.enrollments.push(newEnrollment);

 		return user.save().then(user => true).catch(err => {
 			console.log(err.message, 'this is the message')
 			return err.message})
 	})

 	// if isUserUpdated does not contain the boolean value of true, we will stop our process and reutnr res.send() to our client with our message
 	if(isUserUpdated !== true){
 		return res.send({message: isUserUpdated})
 	}

 	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
 		
 		console.log(course);

 		// create an object which will contain the user id of the enrollee of a course
 		let enrollee = {
 			userId: req.user.id
 		};

 		course.enrollees.push(enrollee);

 		return course.save().then(course => true).catch(err => err.message)

 	});

 	if(isCourseUpdated !== true){
 		return res.send({message: isCourseUpdated})
 	};

 	if(isUserUpdated && isCourseUpdated){
 		return res.send({message: 'Enrolled Successfully'})
 	};

 };


 // get user enrollments

 module.exports.getEnrollments = (req, res) => {

 	// console.log(req.user)

 	User.findById(req.user.id)
 	.then(result => {
 		
 		res.send(result.enrollments)		

 	})
 	.catch(err => res.send(err))

 }