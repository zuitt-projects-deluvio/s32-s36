const express = require('express');
const router = express.Router();

// import user controllers
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

const {verify, verifyAdmin} =  auth;

// Routes

// Create new course
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

// Retrieve all courses
router.get("/", courseControllers.getAllCourses);

// Retrieve a single course with its details
router.get("/getSingleCourse/:id", courseControllers.getSingleCourse);


// Archive a course
router.put("/archive/:id", verify, verifyAdmin, courseControllers.archiveCourse);


// Activate a course
router.put("/activate/:id", verify, verifyAdmin, courseControllers.activateCourse);

// Retrieve active courses
router.get("/getActiveCourse", courseControllers.getActiveCourse);

// Retrieve inactive courses
router.get("/getInactiveCourse", courseControllers.getInactiveCourse);

// Update Course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse)

// find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// find course by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice);


// get course enrollees
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);

module.exports = router;