const express = require('express');

const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

//Routes


// User login
router.post("/login", userControllers.loginUser);

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve all users
router.get("/", userControllers.getAllUsers);

// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

// Check if email exists
router.post("/checkEmailExists", userControllers.checkEmailExists);


// Update user isAdmin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin)


// Update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails)


// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);

// Get enrollments for user
router.get("/getEnrollments", verify, userControllers.getEnrollments);


module.exports = router;